from flask import Flask, jsonify, request, render_template
import requests

app = Flask("EG-PLA-InformeConsolidado", template_folder='templates')

list_type = ["pdf", "doc", "txt"]
list_etapas = ['Inicio', 'Formulación', 'Intermedia', 'Pruebas', 'Fin']
list_modelos = ['Simulación', 'Regresión', 'Clustering', 'Clasificación']
list_entradas = ['Parámetros', 'Dataset']


@app.route('/listarInforme', methods=['GET'])
def listarProyectos():
    return render_template('listarProyectos.html')


@app.route('/crearInforme', methods=['GET'])
def crearProyecto():
    return render_template('crearInforme.html', types=list_type, etapas=list_etapas, modelos=list_modelos, entradas=list_entradas)


@app.route('/guardarInforme', methods=['POST'])
def guardarProyecto():
    informe = dict(request.form)
    requests.post('http://127.0.0.1:5000/informe', json=informe)
    return render_template('listarProyectos.html')

app.run(port=8000, debug=True)
